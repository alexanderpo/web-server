const path = require('path');

module.exports = {
  entry: {
    index: './index.js',
  },
  target: 'node',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'build'),
  },
};
