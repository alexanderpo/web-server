const config = require('config');
const express = require('express');
const vhost = require('vhost');
const path = require('path');


function createVirtualHost({ host, folder }) {
  console.log(host);
  const root = path.resolve(__dirname, path.resolve('sites', folder));
  app.use(vhost(host, express.static(root)));
  app.use(vhost(host, (req, res) => {
    res.sendFile('index.html', { root });
  }));
}

//Create server
var app = express();
console.log('sites:');
config.get('sites').forEach(site => createVirtualHost(site));

console.log('------------------------');

//Start server
var port = config.get('port');
app.listen(port, () => {
  console.log('web server listening on port %d in %s mode', port, app.settings.env);
});